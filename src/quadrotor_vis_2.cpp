/*
 * quadrotor_vis.cpp
 *
 *  Created on: Jun 18, 2015
 *      Author: depardo
 *
 *
 *   visualization of the Quadrotor dynamics. It is based on the acrobot visualization version, but still this is not
 *   based on a base class (as it should).  ToDo : Create a base class.
 *
 */



#include <ros/ros.h>
#include <ros/package.h>
#include <urdf/model.h>
#include <kdl_parser/kdl_parser.hpp>
#include <tf/transform_broadcaster.h>
#include <robot_state_publisher/robot_state_publisher.h>
#include <urdf_parser_plugin/parser.h>

#include <quadrotor_visualization/QuadrotorState.h>

#include <Eigen/Geometry>



// ToDo templated with DIMENSIONS
typedef Eigen::Matrix<double,  7 , 1>  	GeneralizedCoordinates;
typedef Eigen::Matrix<double,  6 , 1>	floating_base_coordinates;
typedef Eigen::Matrix<double, 1, 1> actuated_joint_coordinates;

static std::string package_name = "quadrotor_description";
static const int base_dof = 6;
static const int actuated_dof =1;

static const std::string robot_joint_names[1] =
{
		"fake"

};



void setupJointNames(sensor_msgs::JointState & robot_joints_urdf);
void getRotMatrixFromRPY(const floating_base_coordinates & qb , Eigen::Matrix3d & rot );

// updating global variables directly
void setModelJointPosition(const GeneralizedCoordinates & robot_pose);
void setModelBaseTransformation(const GeneralizedCoordinates & robot_pose);


// updating global variables from messages
void setModelJointFromMessage(const sensor_msgs::JointState &msg);
void setModelBaseStateFromMessage(const geometry_msgs::Pose &msg);


// updating publisher
void updateJointValues(sensor_msgs::JointState & robot_joints_urdf);
void updateBaseTransform(geometry_msgs::TransformStamped & tr);


// global variables to set and get HyQPose
std::map<std::string, double> model_joint_positions;
Eigen::Vector3d floating_base_origin;
Eigen::Quaternion<double> floating_base_rotation_Q;


void poseCallback_2(const quadrotor_visualization::QuadrotorState::ConstPtr & msg )
{  /*const sensor_msgs::JointState &msg*/

	// msg contains the pose twist and joints of the robot

	actuated_joint_coordinates joint_positions;
	floating_base_coordinates base_pose;
	setModelJointFromMessage(msg->joints);
	setModelBaseStateFromMessage(msg->pose);
}



int main(int argc, char** argv)
{
	ros::init(argc, argv, "quadrotor_visualizer_2");
	ros::NodeHandle visualizer_node;
	ros::Subscriber sub = visualizer_node.subscribe("quadrotor_2_pose_topic", 10, &poseCallback_2);

	ros::Rate loop_rate(200);

	std::string urdf_path;
	urdf_path = ros::package::getPath(package_name);

	urdf_path = urdf_path + "/urdf/quadrotor_2.urdf" ;

    	std::cout << "Looking for the robot file in : " << urdf_path << std::endl;


	/* to solve the fix joints problem */
	KDL::Tree my_kdl_tree;
	urdf::Model my_urdf_model;
	bool model_ok  = my_urdf_model.initFile(urdf_path);

	if(!model_ok){
		ROS_ERROR("Invalid URDF File");
		exit(EXIT_FAILURE);
	}

	kdl_parser::treeFromUrdfModel(my_urdf_model, my_kdl_tree);
	robot_state_publisher::RobotStatePublisher state_publisher(my_kdl_tree);


	/* Broadcaster for the floating-base transformation */

	tf::TransformBroadcaster broadcaster;
	geometry_msgs::TransformStamped W_X_B;

	W_X_B.header.frame_id = "WXB";
	W_X_B.child_frame_id = "base_footprint";



	/* Setting up the initial Pose */

	GeneralizedCoordinates robot_canonical_pose;
	robot_canonical_pose.setZero();

	setModelJointPosition(robot_canonical_pose);
	setModelBaseTransformation(robot_canonical_pose);

	while(ros::ok())
	{


	  /* update state with global variables */

	  updateBaseTransform(W_X_B);
	  W_X_B.header.stamp = ros::Time::now();


	  /* publish! */

	  broadcaster.sendTransform(W_X_B);
	  state_publisher.publishTransforms(model_joint_positions, ros::Time::now(),"");
	  state_publisher.publishFixedTransforms("",false);


	  loop_rate.sleep();
	  ros::spinOnce();

  }


  return 0;

};

void setModelJointPosition(const GeneralizedCoordinates & robot_pose)
{

	for(int i = 0 ; i < actuated_dof; i++){
		model_joint_positions[robot_joint_names[i]] = robot_pose[base_dof + i];
	}

}

void setModelBaseTransformation(const GeneralizedCoordinates & robot_pose)
{

	int base_position_index = 3;

	for(int i = 0; i < 3 ; i++){
		/* from base frame to world frame */
		floating_base_origin[i] = robot_pose[base_position_index + i];
	}


	Eigen::Matrix3d rot_I_X_B;
	floating_base_coordinates base_pose = robot_pose.segment<base_dof>(0);

	getRotMatrixFromRPY(base_pose , rot_I_X_B);


	// updating global Quaternion from rotation matrix
	floating_base_rotation_Q = Eigen::Quaternion<double>(rot_I_X_B);

}


void setModelJointFromMessage(const sensor_msgs::JointState &msg )
{


	for(int i = 0 ; i < actuated_dof; i++){

		// Joint values are set according to the JointState convention and shifted from the zero_pose
		model_joint_positions[robot_joint_names[i]] = msg.position[i];

	}

}

void setModelBaseStateFromMessage(const geometry_msgs::Pose &msg)
{

	/* q_base (pos) is in world frame */
	floating_base_origin[0] =  msg.position.x;
	floating_base_origin[1] =  msg.position.y;
	floating_base_origin[2] =  msg.position.z;


	double q_x = msg.orientation.x;
	double q_y = msg.orientation.y;
	double q_z = msg.orientation.z;
	double q_w = msg.orientation.w;

	floating_base_rotation_Q = Eigen::Quaternion<double>(q_w , q_x , q_y , q_z);


}


void updateJointValues(sensor_msgs::JointState & joint_msg)
{

	// catching updated values from the global variable

	for(int i = 0 ; i < actuated_dof; i++){
		joint_msg.position[i] = model_joint_positions[robot_joint_names[i]];
	}

}

void updateBaseTransform(geometry_msgs::TransformStamped & trans_msg)
{

	// yeap, I know, probably with a pointer this would not be necessary but like this it is really easy to understand
	trans_msg.transform.translation.x = floating_base_origin[0];
	trans_msg.transform.translation.y = floating_base_origin[1];
	trans_msg.transform.translation.z = floating_base_origin[2];

	trans_msg.transform.rotation.x = floating_base_rotation_Q.x();
	trans_msg.transform.rotation.y = floating_base_rotation_Q.y();
	trans_msg.transform.rotation.z = floating_base_rotation_Q.z();
	trans_msg.transform.rotation.w = floating_base_rotation_Q.w();

}

void getRotMatrixFromRPY(const floating_base_coordinates & base_pose , Eigen::Matrix3d & rot)
{

	// Euler angels, RPY rotation (appropriate units?)
	double roll_angle  = base_pose(0); // roll (psi)
	double pitch_angle = base_pose(1); // pitch (upsilon)
	double yaw_angle   = base_pose(2); // yaw (psi)

	Eigen::AngleAxisd roll_rotation(roll_angle, Eigen::Vector3d::UnitX());
	Eigen::AngleAxisd pitch_rotation(pitch_angle, Eigen::Vector3d::UnitY());
	Eigen::AngleAxisd yaw_rotation(yaw_angle, Eigen::Vector3d::UnitZ());

	//Rotation matrix from single axis rotations
	rot.setZero();
	rot 	=   yaw_rotation * pitch_rotation * roll_rotation;

}



void setupJointNames(sensor_msgs::JointState & hyq_joints_urdf)
{

	hyq_joints_urdf.name.resize(actuated_dof);

	hyq_joints_urdf.position.resize(actuated_dof);

	for(int i = 0 ; i < actuated_dof ; i++)	{
		hyq_joints_urdf.name[i] = robot_joint_names[i];
		hyq_joints_urdf.position[i] = 0.0;
	}

}
