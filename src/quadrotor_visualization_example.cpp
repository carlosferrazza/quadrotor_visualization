/*
 * quadrotor_visualization_example.cpp
 *
 *  Created on: Feb 23, 2016
 *      Author: depardo
 */

// CEREAL
#include <cereal/archives/xml.hpp>
#include <cereal/types/vector.hpp>
#include <cereal/types/Eigen.hpp>

#include <fstream>

#include<quadrotor_visualization/QuadrotorState.h>
#include<Eigen/Core>

#include <ros/ros.h>
#include <ros/package.h>




// you can avoid that using the DIMENSIONS class we use in ADRL, but I avoid to import another package to the visualization
// so you can do it locally
typedef Eigen::Matrix<double, 12, 1> state_vector_t;
typedef std::vector<state_vector_t, Eigen::aligned_allocator<state_vector_t> > state_vector_array_t;


// This can be wrapped in a class and use it in the vis node and in the apps
void setMessageFromRobotState(const state_vector_t & y_state, quadrotor_visualization::QuadrotorState & msg);
void setMessageFromJointStates(sensor_msgs::JointState &msg );
void setMessageFromBasePose(const state_vector_t & y_state , geometry_msgs::Pose &msg);
void getRotMatrixFromRPY(const Eigen::Vector3d & base_pose , Eigen::Matrix3d & rot);

static const std::string robot_joint_names[1] =
{
		"fake"
};


int main(int argc, const char*argv[])
{

	//Ros Node
	int rosargc = 0;
	char ** rosargv = NULL;
	ros::init(rosargc,rosargv,"TrajectoryPlayer");

	ros::NodeHandle robot_simulation_ros_node;
	ros::NodeHandle robot_simulation_ros_node_2;
	ros::Publisher state_publisher = robot_simulation_ros_node.advertise<quadrotor_visualization::QuadrotorState>("quadrotor_pose_topic",100);
	ros::Publisher state_publisher_2 = robot_simulation_ros_node_2.advertise<quadrotor_visualization::QuadrotorState>("quadrotor_2_pose_topic",100);
	ros::Rate loop_rate(100);


	//Loading the trajectory from file
	state_vector_array_t x_trajectory;
	std::string file_name="eigenVector.xml";
	std::ifstream inXML(file_name.c_str());
	cereal::XMLInputArchive archive_i_xml(inXML);
	archive_i_xml(cereal::make_nvp("vector",x_trajectory));

	state_vector_array_t x_trajectory_2;
	std::string file_name_2="eigenVector_replanned.xml";
	std::ifstream inXML_2(file_name_2.c_str());
	cereal::XMLInputArchive archive_i_xml_2(inXML_2);
	archive_i_xml_2(cereal::make_nvp("vector",x_trajectory_2));

	int i;
	//Publishing the visualization message
	for(i = 0 ; i < x_trajectory.size() ; i++){
		std::cout << i << std::endl;

		quadrotor_visualization::QuadrotorState msg_visualization;
		state_vector_t state_tobe_published = x_trajectory[i];
		setMessageFromRobotState(state_tobe_published,msg_visualization);
		state_publisher.publish(msg_visualization);

		quadrotor_visualization::QuadrotorState msg_visualization_2;
		state_vector_t state_tobe_published_2 = x_trajectory_2[i];
		setMessageFromRobotState(state_tobe_published_2,msg_visualization_2);
		state_publisher_2.publish(msg_visualization_2);

		ros::spinOnce();
		loop_rate.sleep();

		//if your trajectory has a lot of points you don't need this. In this case the trajectory has few points
		//so I have added a pause to see it in action.
		ros::Duration(0.2).sleep();


		//Here you can add markers to plot the trajectory in rviz
	}

	while ( i < x_trajectory_2.size()) {
		quadrotor_visualization::QuadrotorState msg_visualization_2;
		state_vector_t state_tobe_published_2 = x_trajectory_2[i];
		setMessageFromRobotState(state_tobe_published_2,msg_visualization_2);
		state_publisher_2.publish(msg_visualization_2);

		ros::spinOnce();
		loop_rate.sleep();
		++i;
		//if your trajectory has a lot of points you don't need this. In this case the trajectory has few points
		//so I have added a pause to see it in action.
		ros::Duration(0.2).sleep();
	}
}


//ToDo : Move this to a class
void setMessageFromRobotState(const state_vector_t & y_state, quadrotor_visualization::QuadrotorState & msg)
{

	setMessageFromJointStates(msg.joints);
	setMessageFromBasePose(y_state,msg.pose);


}

void setMessageFromJointStates(sensor_msgs::JointState &msg )
{

	for(int i = 0 ; i < 1 ; i++){

		msg.name.push_back(robot_joint_names[i]);
		msg.position.push_back(0.0);
	}

}

void setMessageFromBasePose(const state_vector_t & y_state , geometry_msgs::Pose &msg)
{
	msg.position.x = y_state(0);
	msg.position.y = y_state(1);
	msg.position.z = y_state(2);

	Eigen::Matrix3d rot;
	Eigen::Vector3d rpy = y_state.segment<3>(3);

	getRotMatrixFromRPY(rpy,rot);

	Eigen::Quaternion<double> robot_orientation_Q(rot);

	msg.orientation.x = robot_orientation_Q.x();
	msg.orientation.y = robot_orientation_Q.y();
	msg.orientation.z = robot_orientation_Q.z();
	msg.orientation.w = robot_orientation_Q.w();

}

void getRotMatrixFromRPY(const Eigen::Vector3d & base_pose , Eigen::Matrix3d & rot)
{

	// Euler angels, RPY rotation (appropriate units?)
	double roll_angle  = base_pose(0); // roll (psi)
	double pitch_angle = base_pose(1); // pitch (upsilon)
	double yaw_angle   = base_pose(2); // yaw (psi)

	Eigen::AngleAxisd roll_rotation(roll_angle, Eigen::Vector3d::UnitX());
	Eigen::AngleAxisd pitch_rotation(pitch_angle, Eigen::Vector3d::UnitY());
	Eigen::AngleAxisd yaw_rotation(yaw_angle, Eigen::Vector3d::UnitZ());

	//Rotation matrix from single axis rotations
	rot.setZero();
	rot 	=   yaw_rotation * pitch_rotation * roll_rotation;

}
